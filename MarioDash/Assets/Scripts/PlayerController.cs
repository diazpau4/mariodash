using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed = 5f; 
    public float jumpForce = 10f;
    public GameObject star, Pinchoo;

    private Rigidbody2D rb; 
    private bool isGrounded; 

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); 
    }

    void Update()
    {

        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            isGrounded = false;
        }


    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Ground")
        {
            isGrounded = true;
        }

        if(collision.gameObject.tag=="Star")
        {
            speed = 10;
            star.SetActive(false);
        }

        if (collision.gameObject.tag == "Pincho")
        {
            //SceneManager.LoadScene("SampleScene");

            Pinchoo.GetComponent<AudioSource>().enabled = true;
        }

    }
       
    
}


